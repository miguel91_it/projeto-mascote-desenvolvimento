/*
    Esta lib tem o objetivo de receber uma SpriteSheet em PNG e um arquivo JSON correspondente
    para poder exibir a animacao no browser.

    O arquivo PNG contera os diferentes frames da animacao e o JSON contera a ordem que os frames deverao
    ser executados e os dados de cada frame como tamanho (largura e altura) e posicao dentro do PNG.

    Criado por Joao Miguel 31/10/2017

    **Pontos a desenvolver:
        Receber os caminhos do JSON e do PNG por parametro
*/
$(document).ready(function(){
    
    /*Bloco Abaixo (nao mexer):
        Insere AnimationFrame e CancelAnimationFrame caso o navegador nao possua ainda
        O Chrome (dos testes) ja possui e, portanto, esse bloco de codigo eh inutil
        AnimationFrame eh essencial para a animacao
    */
    (function() {
        // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
        // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
        // requestAnimationFrame polyfill by Erik MÃ¶ller. fixes from Paul Irish and Tino Zijdel
        // MIT license
    
        var lastTime = 0;
        var vendors = ['ms', 'moz', 'webkit', 'o'];
        for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
            window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] 
                                       || window[vendors[x]+'CancelRequestAnimationFrame'];
        }
     
        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = function(callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
                  timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };
     
        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = function(id) {
                clearTimeout(id);
            };
    }());

    /*
        ESCOPO GLOBAL DA BIBLIOTECA 
    */
    var animacao = {
        rodar: false
    };

    canvasMascote = document.getElementById('ssPVAssustado');
    
    canvasMascote.onmouseover = function (){
        
        if (animacao.rodar == false) {
            animacao.rodar = true;
            iniciaAnimacao('ssPVAssustado'); 
        }
    };

    canvasMascote.onmouseleave = function (){    
        
        if (animacao.rodar == true)
            animacao.rodar = false;
    };

 
    
    function iniciaAnimacao(idMascote){
        
        var canvas; //canvas do HTML para renderizar a animacao
        var canvasContext; //contexto do canvas
        var mascoteImagem; //imagem (spritesheet) PNG da animacao
        //var mascote; //estrutra com dados de cada sprite (frame) da animacao
        var sprite_width = 0; //largura do sprite para setar o canvas
        var sprite_height = 0; //altura do sprite para setar o canvas
        var frames = []; //lista de frames extraidos do JSON
        
        //funcao sprite contem os dados de cada frame/sprite a ser renderizado instantaneamente
        function sprite(options) {
            
            var that = {},
                frameIndex = 0,
                tickCount = 0,
                ticksPerFrame = options.ticksPerFrame || 0,
                numberOfFrames = options.numberOfFrames || 1;
                stopAnim = false;

            that.context = options.context;
            that.width = options.width;
            that.height = options.height;
            that.image = options.image;
            that.startX = options.startX || 0;
            that.startY = options.startY || 0;

            //renderiza os frames
            that.renderiza = function() {
                //limpa a renderizacao anterior para nao haver sobreposicao entre renderizacoes
                that.context.clearRect(0, 0, that.width, that.height);

                //renderiza o novo frame
                that.context.drawImage(that.image, that.startX, that.startY, that.width, that.height, 0, 0, that.width, that.height);
            };

            //update controla o intervalo entre renderizacoes sucessivas e controla o indice do proximo frame a ser renderizado
            that.atualiza = function() {

                tickCount += 1;
                
                if (tickCount > ticksPerFrame) {
    
                    tickCount = 0;
                    
                    // If the current frame index is in range
                    if (frameIndex < numberOfFrames - 1) {	
                        // Go to the next frame
                        frameIndex += 1;
                    } else {
                        frameIndex = 0;
                    }

                    atualizaFrame(frameIndex);
                }
            };

            //busca a origem do proximo frame a ser renderizado
            atualizaFrame = function (frameIndex) {

                that.startX = frames[frameIndex].x;
                that.startY = frames[frameIndex].y;
            };

            that.stop = function(){
                
                stopAnim = true;
                
            };

            return that;
        };

        //carrega a imagem do computador
        mascoteImagem = new Image();
        mascoteImagem.src = "./" + idMascote+ ".png";

        canvas = document.getElementById(idMascote);
    
        canvasContext = canvas.getContext("2d");
         
        function animacaoLoop () {
            
            if (animacao.rodar) {
                window.requestAnimationFrame(animacaoLoop);
        
                mascote.renderiza();
                mascote.atualiza();

            }

        }
       
        //le o arquivo JSON e quando retornar os dados
        //preenche a lista de frames e atualiza o tamanho do canvas
        $.getJSON("./" + idMascote + ".json", function(dataJSON){
            
            $.each(dataJSON["frames"], function(key, value){
            
                frames.push(value.frame);

                if (sprite_height == 0 && sprite_width == 0) {
                    
                    sprite_width = value.sourceSize.w;
                    sprite_height = value.sourceSize.h;

                    canvas.width = sprite_width;
                    canvas.height = sprite_height;

                }

            });

            //assim que os dados retornarem, instancia o mascote pela funcao sprite
            mascote = sprite({
                context: canvasContext,
                width: sprite_width,
                height: sprite_height,
                image: mascoteImagem,
                numberOfFrames: frames.length || 0,
                ticksPerFrame: 2  //tempo de duracao de um frame
            });
            console.log(mascote);
            animacaoLoop();
            
        });

    //} ());
    };
    
});

